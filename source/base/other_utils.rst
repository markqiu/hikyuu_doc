.. py:currentmodule:: hikyuu
.. highlightlang:: python

杂项或辅助
=============

函数
------

.. py:function:: roundUp(arg1[, arg2=0])

    向上截取，如10.1截取后为11
    
    :param float arg1: 待处理数据
    :param int arg2: 保留小数位数
    :return: 处理过的数据


.. py:function:: roundDown(arg1[, arg2=0])

    向下截取，如10.1截取后为10
    
    :param float arg1: 待处理数据
    :param int arg2: 保留小数位数
    :return: 处理过的数据
    
    
.. py:function:: getDateRange(start, end)

    获取指定 [start, end) 日期时间范围的自然日日历日期列表，仅支持到日
    
    :param Datetime start: 起始日期
    :param Datetime end: 结束日期
    :rtype: DatetimeList

    
.. py:function:: toPriceList(arg)

    将Python的可迭代对象如 list、tuple 转化为 PriceList
    
    :param arg: 待转化的Python序列
    :rtype: PriceList
    

    
类
-----------
    
.. py:class:: PriceList

    价格序列，其中价格使用double表示，对应C++中的std::vector<double>。

    .. py:method:: to_np()

        仅在安装了numpy模块时生效，转换为numpy.array

    .. py:method:: to_df()

        仅在安装了pandas模块时生效，转换为pandas.DataFrame
        
        
.. py:class:: DatetimeList

    日期序列，对应C++中的std::vector<Datetime>
    
    .. py:method:: to_np()

        仅在安装了numpy模块时生效，转换为numpy.array

    .. py:method:: to_df()

        仅在安装了pandas模块时生效，转换为pandas.DataFrame


.. py:class:: StringList

    字符串列表，对应C++中的std::vector<String>