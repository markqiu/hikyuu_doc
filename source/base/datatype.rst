.. py:currentmodule:: hikyuu
.. highlightlang:: python

基础数据类型
============

日期时间
-----------

.. py:class:: Datetime

    日期时间类（精确到秒），通过以下方式构建：
    
    - 通过字符串：Datetime("2010-1-1 10:00:00")
    - 通过 Python 的date：Datetime(date(2010,1,1))
    - 通过 Python 的datetime：Datetime(datetime(2010,1,1,10)
    - 通过 YYYYMMDDHHMM 形式的整数：Datetime(201001011000)
    
    获取日期列表参见： :py:func:`getDateRange`
    
    获取交易日日期参见： :py:meth:`StockManager.getTradingCalendar` 

    .. py:attribute:: year 年
    .. py:attribute:: month 月
    .. py:attribute:: day 日
    .. py:attribute:: hour 时
    .. py:attribute:: minute 分
    .. py:attribute:: second 秒
    .. py:attribute:: number YYYYMMDDHHMM 形式的整数
    
    .. py:method:: date()
    
        转化生成 python 的 date
        
    .. py:method:: datetime()
    
        转化生成 python 的datetime
    
    .. py:method:: isNull()
    
        是否是Null值, 即是否等于 constant.null_datetime
    
    .. py:staticmethod:: max()
    
        获取支持的最大日期时间
        
    .. py:staticmethod:: min()
    
        获取支持的最小日期时间
        
    .. py:staticmethod:: now()
    
        获取当前的日期时间


K线数据
----------       
       
.. py:class:: KRecord

    K线记录，组成K线数据

    .. py:attribute:: datetime : 日期时间
    .. py:attribute:: openPrice : 开盘价
    .. py:attribute:: highPrice : 最高价
    .. py:attribute:: lowPrice  : 最低价
    .. py:attribute:: closePrice : 收盘价
    .. py:attribute:: transAmount : 成交金额
    .. py:attribute:: transCount: 成交量

    
.. py:class:: KData

    K线数据，由 KRecord 组成的数组，可象 list 一样进行遍历
    
    .. py:method:: getDatetimeList
    
        :return: 交易日期列表
        :rtype: DatetimeList
        
    .. py:method:: getKRecord(pos)
    
        :param int pos: 位置索引
        :return: 指定位置的K线记录
        :rtype: KRecord
    
    .. py:method:: getKRecordByDate(datetime)
    
        :param Datetime datetime: 指定的日期
        :return: 指定日期的K线记录
        :rtype: KRecord
    
    .. py:method:: empty
    
        :return: 是否为空
        :rtype: bool
    
    .. py:method:: getQuery
    
        :return: 查询条件
        :rtype: KQuery
    
    .. py:method:: getStock
    
        :return: 关联的 Stock
        :rtype: Stock
    
    .. py:method:: tocsv(filename)
    
        :param string filename: 指定保存的文件名称

    .. py:method:: to_np
    
        :return: 转化为numpy结构数组
        :rtype: numpy.array
        
    .. py:method:: to_df
    
        :return: 转化为pandas的DataFrame
        :rtype: pandas.DataFrame        